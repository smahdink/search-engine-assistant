function change(tabs){
    // turns duckduckgo url into google url
    let url = tabs[0].url;
    let len_ddg = 0
    if (url.includes("https://duckduckgo.com"))
    {
        var new_url = url.replace("https://duckduckgo.com/", "https://www.google.com/search");
    }
    else if (url.includes("https://www.google.com/search"))
    {
        new_url = url.replace("https://www.google.com/search", "https://duckduckgo.com/")
    }
    else
    {
        console.log("failed")
        update_popup(false)
        return;
    }
    update_popup(true);
    browser.tabs.update({url: new_url})
}

function update_popup(result)
{
    if (result == true)
    {
        document.getElementById("text").innerHTML="Done";
        document.getElementById("text").style.color="green"
    }
    else
    {
        document.getElementById("text").innerHTML="Failed"
        document.getElementById("text").style.color="red"
    }
}

var querying = browser.tabs.query({currentWindow: true, active: true});
querying.then(change);


